<?php

if(!isset($_SESSION['admin_email'])){

echo "<script>window.open('login.php','_self')</script>";

}

else {


?>
<script src="//cdn.tiny.cloud/1/0jat596wbm2tsqz64t47eaq61d4r8m5fzvfltfaghc0ei90i/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({ selector:'#review_desc' });</script>
<?php

if(isset($_GET['edit_review'])){

$edit_review = $_GET['edit_review'];

$edit_cat = "select * from reviews where id='$edit_review'";

$run_edit = mysqli_query($con,$edit_cat);

$row_edit = mysqli_fetch_array($run_edit);

$customerId = $row_edit['customerId'];
$emailId = $row_edit['emailId'];
$reviewDescription = $row_edit['reviewDescription'];
$productId = $row_edit['productId'];
$status = $row_edit['status'];
$reviewer_photo = $row_edit['reviewer_photo'];
$reviewer_photo_new = $row_edit['reviewer_photo'];

}

?>

<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li>

<i class="fa fa-dashboard"></i> Dashboard / Edit Review

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->


<div class="row"><!-- 2 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title"><!-- panel-title Starts -->

<i class="fa fa-money fa-fw"></i> Edit Review

</h3><!-- panel-title Ends -->

</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

<form class="form-horizontal" action="" method="post" enctype="multipart/form-data"><!-- form-horizontal Starts -->

<div class="form-group"><!-- form-group Starts -->
<label class="col-md-3 control-label">Product Id</label>
<div class="col-md-6">
<input type="text" name="productId" class="form-control" value="<?php echo $productId; ?>">
</div>
</div>

<div class="form-group"><!-- form-group Starts -->
<label class="col-md-3 control-label">Customer Id</label>
<div class="col-md-6">
<input type="text" name="customerId" class="form-control" value="<?php echo $customerId; ?>">
</div>
</div>

<div class="form-group"><!-- form-group Starts -->
<label class="col-md-3 control-label">Customer Email</label>
<div class="col-md-6">
<input type="text" name="emailId" class="form-control" value="<?php echo $emailId; ?>">
</div>
</div>

<div class="form-group"><!-- form-group Starts -->
<label class="col-md-3 control-label">Review Description</label>
<div class="col-md-6">
<textarea name="reviewDescription" class="form-control" rows="15" id="review_desc">
    <?php
        echo $reviewDescription;
    ?>
</textarea>

</div>
</div>

<div class="form-group"><!-- form-group Starts -->
<label class="col-md-3 control-label">Status</label>
<div class="col-md-6">
<input type="radio" name="status" value="pending" required>
<label>Pending</label>
&nbsp; &nbsp; 
<input type="radio" name="status" value="approved" required>
<label>Approved</label>
&nbsp; &nbsp; 
<input type="radio" name="status" value="rejected" required>
<label>Rejected</label>
</div>
</div>

<div class="form-group"><!-- form-group Starts -->
<label class="col-md-3 control-label">Reviewer Photo</label>
<div class="col-md-6">
<input type="file" name="reviewer_photo" class="form-control">

<br>

<img src="other_images/<?php echo $reviewer_photo; ?>" width="70" height="70" >
</div>
</div>




<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label"></label>

<div class="col-md-6">

<input type="submit" name="update" value="Update Review" class="btn btn-primary form-control">

</div>

</div><!-- form-group Ends -->

</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends -->

<?php

if(isset($_POST['update'])){


    $customerId = $_POST['customerId'];
    $emailId = $_POST['emailId'];
    $reviewDescription = $_POST['reviewDescription'];
    $productId = $_POST['productId'];
    $status = $_POST['status'];
    $reviewer_photo = $_FILES['reviewer_photo']['name'];
    $temp_name = $_FILES['reviewer_photo']['tmp_name'];

    $uniqueNumber = time();
    if(empty($reviewer_photo)){
        $reviewer_photo= $reviewer_photo_new;
    } else{
        $reviewer_photo = $uniqueNumber.'_'.$reviewer_photo;
    }


move_uploaded_file($temp_name,"other_images/$reviewer_photo");

$update_cat = "update reviews set customerId='$customerId',emailId='$emailId',reviewDescription='$reviewDescription',productId='$productId',status='$status',reviewer_photo='$reviewer_photo' where id='$edit_review'";

$run_cat = mysqli_query($con,$update_cat);

if($run_cat){

echo "<script>alert('One Review Has Been Updated')</script>";

echo "<script>window.open('index.php?view_reviews','_self')</script>";

}

}



?>

<?php } ?>