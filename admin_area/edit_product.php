<?php

if(!isset($_SESSION['admin_email'])){

echo "<script>window.open('login.php','_self')</script>";

}

else {

?>

<?php

if(isset($_GET['edit_product'])){

$edit_id = $_GET['edit_product'];

$get_p = "select * from products where product_id='$edit_id'";

$run_edit = mysqli_query($con,$get_p);

$row_edit = mysqli_fetch_array($run_edit);

$p_id = $row_edit['product_id'];

$p_title = $row_edit['product_title'];

$p_cat = $row_edit['p_cat_id'];

$cat = $row_edit['cat_id'];

$m_id = $row_edit['manufacturer_id'];

$p_image1 = $row_edit['product_img1'];
$p_image2 = $row_edit['product_img2'];
$p_image3 = $row_edit['product_img3'];
$p_image4 = $row_edit['product_img4'];
$p_image5 = $row_edit['product_img5'];
$p_image6 = $row_edit['product_img6'];
$p_image7 = $row_edit['product_img7'];
$p_image8 = $row_edit['product_img8'];

$new_p_image1 = $row_edit['product_img1'];
$new_p_image2 = $row_edit['product_img2'];
$new_p_image3 = $row_edit['product_img3'];
$new_p_image4 = $row_edit['product_img4'];
$new_p_image5 = $row_edit['product_img5'];
$new_p_image6 = $row_edit['product_img6'];
$new_p_image7 = $row_edit['product_img7'];
$new_p_image8 = $row_edit['product_img8'];

$p_price = $row_edit['product_price'];

$p_desc = $row_edit['product_desc'];

$p_keywords = $row_edit['product_keywords'];

$psp_price = $row_edit['product_psp_price'];

$p_label = $row_edit['product_label'];

$p_url = $row_edit['product_url'];

$p_features = $row_edit['product_features'];

$p_video = $row_edit['product_video'];

$p_offer_price = $row_edit['product_offer_price'];
$p_emi_starts = $row_edit['product_emi_starts'];
$p_is_upcoming = $row_edit['product_is_upcoming'];
$p_status = $row_edit['status'];
}

$get_manufacturer = "select * from manufacturers where manufacturer_id='$m_id'";

$run_manufacturer = mysqli_query($con,$get_manufacturer);

$row_manfacturer = mysqli_fetch_array($run_manufacturer);

$manufacturer_id = $row_manfacturer['manufacturer_id'];

$manufacturer_title = $row_manfacturer['manufacturer_title'];


$get_p_cat = "select * from product_categories where p_cat_id='$p_cat'";

$run_p_cat = mysqli_query($con,$get_p_cat);

$row_p_cat = mysqli_fetch_array($run_p_cat);

$p_cat_title = $row_p_cat['p_cat_title'];

$get_cat = "select * from categories where cat_id='$cat'";

$run_cat = mysqli_query($con,$get_cat);

$row_cat = mysqli_fetch_array($run_cat);

$cat_title = $row_cat['cat_title'];
$isUpcoming = $p_is_upcoming ? 'selected' : '';
?>


<!DOCTYPE html>

<html>

<head>

<title> Edit Products </title>


<script src="//cdn.tiny.cloud/1/0jat596wbm2tsqz64t47eaq61d4r8m5fzvfltfaghc0ei90i/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

  <script>tinymce.init({ selector:'#product_desc,#product_features' });</script>

</head>

<body>

<div class="row"><!-- row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li class="active">

<i class="fa fa-dashboard"> </i> Dashboard / Edit Products

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- row Ends -->


<div class="row"><!-- 2 row Starts --> 

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title">

<i class="fa fa-money fa-fw"></i> Edit Products

</h3>

</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

<form class="form-horizontal" method="post" enctype="multipart/form-data"><!-- form-horizontal Starts -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Title </label>

<div class="col-md-6" >

<input type="text" name="product_title" class="form-control" required value="<?php echo $p_title; ?>">

</div>

</div><!-- form-group Ends -->


<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Url </label>

<div class="col-md-6" >

<input type="text" name="product_url" class="form-control" required value="<?php echo $p_url; ?>" >

<br>

<p style="font-size:15px; font-weight:bold;">

Product Url Example : navy-blue-t-shirt

</p>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Select A Manufacturer </label>

<div class="col-md-6" >

<select name="manufacturer" class="form-control">

<option value="<?php echo $manufacturer_id; ?>">
<?php echo $manufacturer_title; ?>
</option>

<?php

$get_manufacturer = "select * from manufacturers";

$run_manufacturer = mysqli_query($con,$get_manufacturer);

while($row_manfacturer = mysqli_fetch_array($run_manufacturer)){

$manufacturer_id = $row_manfacturer['manufacturer_id'];

$manufacturer_title = $row_manfacturer['manufacturer_title'];

echo "
<option value='$manufacturer_id'>
$manufacturer_title
</option>
";

}

?>

</select>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Category </label>

<div class="col-md-6" >

<select name="product_cat" class="form-control" >

<option value="<?php echo $p_cat; ?>" > <?php echo $p_cat_title; ?> </option>


<?php

$get_p_cats = "select * from product_categories";

$run_p_cats = mysqli_query($con,$get_p_cats);

while ($row_p_cats=mysqli_fetch_array($run_p_cats)) {

$p_cat_id = $row_p_cats['p_cat_id'];

$p_cat_title = $row_p_cats['p_cat_title'];

echo "<option value='$p_cat_id' >$p_cat_title</option>";

}


?>


</select>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Category </label>

<div class="col-md-6" >


<select name="cat" class="form-control" >

<option value="<?php echo $cat; ?>" > <?php echo $cat_title; ?> </option>

<?php

$get_cat = "select * from categories ";

$run_cat = mysqli_query($con,$get_cat);

while ($row_cat=mysqli_fetch_array($run_cat)) {

$cat_id = $row_cat['cat_id'];

$cat_title = $row_cat['cat_title'];

echo "<option value='$cat_id'>$cat_title</option>";

}

?>


</select>

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Image 1 </label>

<div class="col-md-6" style="display:flex">

<input type="file" name="product_img1" class="form-control" > <img src="product_images/small/<?php echo $p_image1; ?>" width="50" height="50" >

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Image 2 </label>

<div class="col-md-6" style="display:flex">

<input type="file" name="product_img2" class="form-control" >
<img src="product_images/small/<?php echo $p_image2; ?>" width="50" height="50" >

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Image 3 </label>

<div class="col-md-6" style="display:flex">

<input type="file" name="product_img3" class="form-control" >
<img src="product_images/small/<?php echo $p_image3; ?>" width="50" height="50" >

</div>

</div><!-- form-group Ends -->


<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Image 4 </label>

<div class="col-md-6" style="display:flex">

<input type="file" name="product_img4" class="form-control" >
<img src="product_images/small/<?php echo $p_image4; ?>" width="50" height="50" >

</div>

</div><!-- form-group Ends -->


<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Image 5 </label>

<div class="col-md-6" style="display:flex">

<input type="file" name="product_img5" class="form-control" >
<img src="product_images/small/<?php echo $p_image5; ?>" width="50" height="50" >

</div>

</div><!-- form-group Ends -->


<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Image 6 </label>

<div class="col-md-6" style="display:flex">

<input type="file" name="product_img6" class="form-control" >
<img src="product_images/small/<?php echo $p_image6; ?>" width="50" height="50" >

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Image 7 </label>

<div class="col-md-6" style="display:flex">

<input type="file" name="product_img7" class="form-control" >
<img src="product_images/small/<?php echo $p_image7; ?>" width="50" height="50" >

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Image 8 </label>

<div class="col-md-6" style="display:flex">

<input type="file" name="product_img8" class="form-control" >
<img src="product_images/small/<?php echo $p_image8; ?>" width="50" height="50" >

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Price </label>

<div class="col-md-6" >

<input type="text" name="product_price" class="form-control" required value="<?php echo $p_price; ?>" >

</div>

</div><!-- form-group Ends -->



<div class="form-group" ><!-- form-group Starts -->
<label class="col-md-3 control-label" > Product Offer Price </label>
<div class="col-md-6" >
<input type="text" name="product_offer_price" class="form-control" value="<?php echo $p_offer_price; ?>" >
</div>
</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->
<label class="col-md-3 control-label" > Product EMI Starts </label>
<div class="col-md-6" >
<input type="text" name="product_emi_starts" class="form-control" value="<?php echo $p_emi_starts; ?>" >
</div>
</div><!-- form-group Ends -->

<!-- form-group Ends -->

<!--<div class="form-group" >
<label class="col-md-3 control-label" > Product Sale Price </label>
<div class="col-md-6" >
<input type="text" name="psp_price" class="form-control" required value="<?php echo $psp_price; ?>">
</div>
</div>-->

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Keywords </label>

<div class="col-md-6" >

<input type="text" name="product_keywords" class="form-control"  value="<?php echo $p_keywords; ?>" >

</div>

</div><!-- form-group Ends -->
<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Label </label>

<div class="col-md-6" >

<input type="text" name="product_label" class="form-control"  value="<?php echo $p_label; ?>">

</div>

</div><!-- form-group Ends -->

<div class="form-group" ><!-- form-group Starts -->
<label class="col-md-3 control-label" > Is Upcoming </label>
<div class="col-md-6" >
  <select name="product_is_upcoming" class="form-control">
    <option value="">Select</option>
    <option value="upcoming" <?php echo $isUpcoming; ?>>Upcoming</option>
  </select>
</div>
</div>

<div class="form-group" ><!-- form-group Starts -->
<label class="col-md-3 control-label" > Product Status </label>
<div class="col-md-6" >
  <select name="status"  required class="form-control">
    <option value="" value="">Select</option>
    <option value="approved" <?php echo $p_status === 'approved' ? 'selected' : '' ; ?>>Approved</option>
    <option value="pending" <?php echo $p_status === 'pending' ? 'selected' : '' ; ?>>Pending</option>
    <option value="rejected" <?php echo $p_status === 'rejected' ? 'selected' : '' ; ?>>Rejected</option>
  </select>
</div>
</div>

<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" > Product Tabs </label>

<div class="col-md-6" >

<ul class="nav nav-tabs"><!-- nav nav-tabs Starts -->

<li class="active">

<a data-toggle="tab" href="#description"> Product Description </a>

</li>

<li>

<a data-toggle="tab" href="#features"> Product Features </a>

</li>

<li>

<a data-toggle="tab" href="#video"> Sounds And Videos </a>

</li>

</ul><!-- nav nav-tabs Ends -->

<div class="tab-content"><!-- tab-content Starts -->

<div id="description" class="tab-pane fade in active"><!-- description tab-pane fade in active Starts -->

<br>

<textarea name="product_desc" class="form-control" rows="15" id="product_desc">

<?php echo $p_desc; ?>

</textarea>

</div><!-- description tab-pane fade in active Ends -->


<div id="features" class="tab-pane fade in"><!-- features tab-pane fade in Starts -->
  
 <div class="row featurerow">
  <div class="col-md-6" >
    <b>Select Feature Heading</b>
  </div>
  <div class="col-md-6" >
    <b>Select Feature Value</b>
  </div>
</div>  
<div id="pFeatures">

 <?php
 
  $get_features = "select a.productId, a.headingId, a.valueId, b.value heading, c.value value from features a, featureheadings b, featurevalues c where a.productId = '$edit_id' and b.id=a.headingId and c.id=a.valueId";
  $run_features = mysqli_query($con,$get_features);
    while($row_features = mysqli_fetch_array($run_features)){
      echo '<div class="row featurerow"> <div class="col-md-6" > <input type="text" class="feature-heading form-control" name="featureHeading[]" class="form-control" value="'.$row_features['heading'].'" > <input type="hidden" name="featureHeadingId[]" value="'.$row_features['headingId'].'" > </div> <div class="col-md-6" > <input type="text" name="featureValue[]" class="feature-values form-control" value="'.$row_features['value'].'" > <input type="hidden" name="featureValueId[]" class="feature-values form-control" value="'.$row_features['valueId'].'" > </div> </div>';
    }
?>
</div>
<button class="add-feature" type="button">Add More</button>

</div><!-- features tab-pane fade in Ends -->


<div id="video" class="tab-pane fade in"><!-- video tab-pane fade in Starts -->

<br>

<textarea name="product_video" class="form-control" rows="15">

<?php echo $p_video; ?>

</textarea>

</div><!-- video tab-pane fade in Ends -->


</div><!-- tab-content Ends -->

</div>

</div><!-- form-group Ends -->



<div class="form-group" ><!-- form-group Starts -->

<label class="col-md-3 control-label" ></label>

<div class="col-md-6" >

<input type="submit" name="update" value="Update Product" class="btn btn-primary form-control" >

</div>

</div><!-- form-group Ends -->

</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends --> 




</body>

</html>

<?php

function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80){
  $imgsize = getimagesize($source_file);
  $width = $imgsize[0];
  $height = $imgsize[1];
  $mime = $imgsize['mime'];

  switch($mime){
      case 'image/gif':
          $image_create = "imagecreatefromgif";
          $image = "imagegif";
          break;

      case 'image/png':
          $image_create = "imagecreatefrompng";
          $image = "imagepng";
          $quality = 7;
          break;

      case 'image/jpeg':
          $image_create = "imagecreatefromjpeg";
          $image = "imagejpeg";
          $quality = 80;
          break;

      default:
          return false;
          break;
  }

  $dst_img = imagecreatetruecolor($max_width, $max_height);
  $src_img = $image_create($source_file);

  $width_new = $height * $max_width / $max_height;
  $height_new = $width * $max_height / $max_width;
  //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
  if($width_new > $width){
      //cut point by height
      $h_point = (($height - $height_new) / 2);
      //copy image
      imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
  }else{
      //cut point by width
      $w_point = (($width - $width_new) / 2);
      imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
  }

  $image($dst_img, $dst_dir, $quality);

  if($dst_img)imagedestroy($dst_img);
  if($src_img)imagedestroy($src_img);
}

if(isset($_POST['update'])){

$product_title = $_POST['product_title'];
$product_cat = $_POST['product_cat'];
$cat = $_POST['cat'];
$manufacturer_id = $_POST['manufacturer'];
$product_price = $_POST['product_price'];
$product_desc = $_POST['product_desc'];
$product_keywords = $_POST['product_keywords'];

$psp_price = $_POST['psp_price'];

$product_label = $_POST['product_label'];

$product_url = $_POST['product_url'];

$product_features = $_POST['product_features'];

$product_video = $_POST['product_video'];

$status = $_POST['status'];
$product_is_upcoming = $_POST['product_is_upcoming'];
$featureValueIds = $_POST['featureValueId'];
$featureHeadingIds = $_POST['featureHeadingId'];

$product_img1 = $_FILES['product_img1']['name'];
$product_img2 = $_FILES['product_img2']['name'];
$product_img3 = $_FILES['product_img3']['name'];
$product_img4 = $_FILES['product_img4']['name'];
$product_img5 = $_FILES['product_img5']['name'];
$product_img6 = $_FILES['product_img6']['name'];
$product_img7 = $_FILES['product_img7']['name'];
$product_img8 = $_FILES['product_img8']['name'];

$temp_name1 = $_FILES['product_img1']['tmp_name'];
$temp_name2 = $_FILES['product_img2']['tmp_name'];
$temp_name3 = $_FILES['product_img3']['tmp_name'];
$temp_name4 = $_FILES['product_img4']['tmp_name'];
$temp_name5 = $_FILES['product_img5']['tmp_name'];
$temp_name6 = $_FILES['product_img6']['tmp_name'];
$temp_name7 = $_FILES['product_img7']['tmp_name'];
$temp_name8 = $_FILES['product_img8']['tmp_name'];

$uniqueNumber = time();

if(empty($product_img1)){
  $product_img1 = $new_p_image1;
} else{
  $product_img1 =  $uniqueNumber."_view1_".$product_img1;
}
if(empty($product_img2)){
  $product_img2 = $new_p_image2;
}else {
  $product_img2 =  $uniqueNumber."_view2_".$product_img2;
}
if(empty($product_img3)){
  $product_img3 = $new_p_image3;
}else {
  $product_img3 =  $uniqueNumber."_view3_".$product_img3;
}
if(empty($product_img4)){
  $product_img4 = $new_p_image4;
} else {
  $product_img4 =  $uniqueNumber."_view4_".$product_img4;
}
if(empty($product_img5)){
  $product_img5 = $new_p_image5;
} else {
  $product_img5 =  $uniqueNumber."_view5_".$product_img5;
}
if(empty($product_img6)){
  $product_img6 = $new_p_image6;
} else {
  $product_img6 =  $uniqueNumber."_view6_".$product_img6;
}
if(empty($product_img7)){
  $product_img7 = $new_p_image7;
} else {
  $product_img7 =  $uniqueNumber."_view7_".$product_img7;
}
if(empty($product_img8)){
  $product_img8 = $new_p_image8;
} else {
  $product_img8 =  $uniqueNumber."_view8_".$product_img8;
}

$imageName1 = $product_img1;
$imageName2 = $product_img2;
$imageName3 = $product_img3;
$imageName4 = $product_img4;
$imageName5 = $product_img5;
$imageName6 = $product_img6;
$imageName7 = $product_img7;
$imageName8 = $product_img8;

move_uploaded_file($temp_name1,"product_images/large/$imageName1");
move_uploaded_file($temp_name2,"product_images/large/$imageName2");
move_uploaded_file($temp_name3,"product_images/large/$imageName3");
move_uploaded_file($temp_name4,"product_images/large/$imageName4");
move_uploaded_file($temp_name5,"product_images/large/$imageName5");
move_uploaded_file($temp_name6,"product_images/large/$imageName6");
move_uploaded_file($temp_name7,"product_images/large/$imageName7");
move_uploaded_file($temp_name8,"product_images/large/$imageName8");

resize_crop_image(100, 100, "product_images/large/$imageName1", "product_images/small/$imageName1");
resize_crop_image(100, 100, "product_images/large/$imageName2", "product_images/small/$imageName2");
resize_crop_image(100, 100, "product_images/large/$imageName3", "product_images/small/$imageName3");
resize_crop_image(100, 100, "product_images/large/$imageName4", "product_images/small/$imageName4");
resize_crop_image(100, 100, "product_images/large/$imageName5", "product_images/small/$imageName5");
resize_crop_image(100, 100, "product_images/large/$imageName6", "product_images/small/$imageName6");
resize_crop_image(100, 100, "product_images/large/$imageName7", "product_images/small/$imageName7");
resize_crop_image(100, 100, "product_images/large/$imageName8", "product_images/small/$imageName8");

$update_product = "update products set p_cat_id='$product_cat',cat_id='$cat',manufacturer_id='$manufacturer_id',date=NOW(),product_title='$product_title',product_url='$product_url',product_img1='$imageName1',product_img2='$imageName2',product_img3='$imageName3',product_img4='$imageName4',product_img5='$imageName5',product_img6='$imageName6',product_img7='$imageName7',product_img8='$imageName8',product_price='$product_price',product_psp_price='$psp_price',product_desc='$product_desc',product_features='$product_features',product_video='$product_video',product_keywords='$product_keywords',product_label='$product_label',status='$status', product_is_upcoming='$product_is_upcoming' where product_id='$p_id'";

$run_product = mysqli_query($con,$update_product);

$delete_features = "delete from features where productId='$p_id'";
$run_delete_features = mysqli_query($con,$delete_features);

if($run_delete_features) {
  $featureCounter = 0;
  $insert_features = "insert into features (productId, headingId, valueId) values ";
  forEach($featureValueIds as $valueId) {
    $headingId = $featureHeadingIds[$featureCounter];
    if( $headingId && $valueId ) {
    $insert_features .= "('$p_id','$headingId', '$valueId')";
    }
    $featureCounter++;
    if( $headingId && $valueId ) {
      $insert_features .= count($featureValueIds) === $featureCounter ? ';' : ',';
    } 
  }
  $run_feature = mysqli_query($con,$insert_features);
}

if($run_product){

echo "<script> alert('Product has been updated successfully') </script>";

echo "<script>window.open('index.php?view_products','_self')</script>";

}

}

?>

<?php } ?>
