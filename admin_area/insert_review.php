<?php

if(!isset($_SESSION['admin_email'])){

echo "<script>window.open('login.php','_self')</script>";

}

else {


?>
<script src="//cdn.tiny.cloud/1/0jat596wbm2tsqz64t47eaq61d4r8m5fzvfltfaghc0ei90i/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({ selector:'#review_desc' });</script>
<div class="row"><!-- 1 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<ol class="breadcrumb"><!-- breadcrumb Starts -->

<li>

<i class="fa fa-dashboard"></i> Dashboard / Insert Review

</li>

</ol><!-- breadcrumb Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 1 row Ends -->


<div class="row"><!-- 2 row Starts -->

<div class="col-lg-12"><!-- col-lg-12 Starts -->

<div class="panel panel-default"><!-- panel panel-default Starts -->

<div class="panel-heading"><!-- panel-heading Starts -->

<h3 class="panel-title"><!-- panel-title Starts -->

<i class="fa fa-money fa-fw"></i> Insert Review

</h3><!-- panel-title Ends -->

</div><!-- panel-heading Ends -->

<div class="panel-body"><!-- panel-body Starts -->

<form class="form-horizontal" action="" method="post" enctype="multipart/form-data"><!-- form-horizontal Starts -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">Product Id</label>

<div class="col-md-6">

<input type="text" name="productId" class="form-control">

</div>

</div>

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">Customer Id</label>

<div class="col-md-6">

<input type="text" name="customerId" class="form-control">

</div>

</div>

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">Customer Email</label>

<div class="col-md-6">

<input type="text" name="emailId" class="form-control">

</div>

</div>

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">Review Description</label>

<div class="col-md-6">

<textarea name="reviewDescription" class="form-control" rows="15" id="review_desc">
</textarea>

</div>

</div>
<!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">Status</label>

<div class="col-md-6">

<input type="radio" name="status" value="pending">
<label>Pending</label>
&nbsp; &nbsp; 
<input type="radio" name="status" value="approved">
<label>Approved</label>
&nbsp; &nbsp; 
<input type="radio" name="status" value="rejected">
<label>Rejected</label>

</div>

</div><!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label">Reviewer Photo</label>

<div class="col-md-6">

<input type="file" name="reviewer_photo" class="form-control">

</div>

</div><!-- form-group Ends -->

<div class="form-group"><!-- form-group Starts -->

<label class="col-md-3 control-label"></label>

<div class="col-md-6">

<input type="submit" name="submit" value="Insert Review" class="btn btn-primary form-control">

</div>

</div><!-- form-group Ends -->

</form><!-- form-horizontal Ends -->

</div><!-- panel-body Ends -->

</div><!-- panel panel-default Ends -->

</div><!-- col-lg-12 Ends -->

</div><!-- 2 row Ends -->

<?php

if(isset($_POST['submit'])){

$customerId = $_POST['customerId'];
$emailId = $_POST['emailId'];
$reviewDescription = $_POST['reviewDescription'];
$productId = $_POST['productId'];
$status = $_POST['status'];
$uniqueNumber = time();

$reviewer_photo = $_FILES['reviewer_photo']['name'];
$temp_name = $_FILES['reviewer_photo']['tmp_name'];
$reviewer_photo = $uniqueNumber.'_'.$reviewer_photo;
move_uploaded_file($temp_name,"other_images/$reviewer_photo");

$insert_cat = "insert into reviews (customerId,emailId,reviewDescription,productId,status,reviewer_photo) values ('$customerId','$emailId','$reviewDescription','$productId', '$status', '$reviewer_photo')";

$run_cat = mysqli_query($con,$insert_cat);

if($run_cat){

echo "<script> alert('Review Has Been Inserted')</script>";

echo "<script> window.open('index.php?view_reviews','_self') </script>";

}

}



?>

<?php } ?>