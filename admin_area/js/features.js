$(function () {

    function initAutoComplete() {
        $(".feature-heading").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "features_heading.php",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        response($.map(data.data, function (item) {
                            return {
                                id: item.id,
                                value: item.value
                            };
                        }));
                    }
                });
            },
            select: function (event, ui) {
                console.log($(this).attr('class'));
                $(this).val(ui.item.value); // display the selected text
                $(this).next('input').val(ui.item.id); // save selected id to hidden input
                return false;
            },
            change: function (event, ui) {
                // $( "#tags_id" ).val( ui.item? ui.item.id : 0 );
            }
        });

        $(".feature-values").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "features_values.php",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        response($.map(data.data, function (item) {
                            return {
                                id: item.id,
                                value: item.value
                            };
                        }));
                    }
                });
            },
            select: function (event, ui) {
                console.log($(this).attr('class'));
                $(this).val(ui.item.value); // display the selected text
                $(this).next('input').val(ui.item.id); // save selected id to hidden input
                return false;
            },
            change: function (event, ui) {
                // $( "#tags_id" ).val( ui.item? ui.item.id : 0 );
            }
        });
    }

    function distroyAutoComplete() {
        $(".feature-values").autocomplete("destroy");
        $(".feature-heading").autocomplete("destroy");
    }
    $(document).on("blur", ".feature-heading", function () {
        if ($(this).val() === '') {
            $(this).next('input').val('')
        }
    })
    $(document).on("blur", ".feature-values", function () {
        if ($(this).val() === '') {
            $(this).next('input').val('')
        }
    })

    $(document).on("click", ".delete-feature", function () {
        $(this).parent('.featurerow').remove();
    })

    $(document).on('click', '.add-feature', function () {
        // distroyAutoComplete();
        
        $('#pFeatures').append('<div class="row featurerow"><button class="delete-feature" type="button">X</button> <div class="col-md-6" > <input type="text" class="feature-heading form-control" name="featureHeading[]" class="form-control" value="" > <input type="hidden" name="featureHeadingId[]" value="" > </div> <div class="col-md-6" > <input type="text" name="featureValue[]" class="feature-values form-control" value="" > <input type="hidden" name="featureValueId[]" class="feature-values form-control" value="" > </div> </div>');
        initAutoComplete();
    })


    initAutoComplete();
});